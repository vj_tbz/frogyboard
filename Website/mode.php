<?php 
session_start();

if (!isset($_SESSION['email'])) {
    // Redirect to the login page or show an error
    header("Location: login.html");
    exit;
}

?>

<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="utf-8">
		<title>FROGYBOARD - Gamemode</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/background.css" type="text/css">
		<link rel="stylesheet" href="assets/css/titlescreen.css" type="text/css">
		<link rel="stylesheet" href="assets/css/mode.css" type="text/css">
	</head>



	<body>
		<header>
			<p> </p>
		</header>


		<nav class="mainnav">
			<ul>
				<li><a class="log" href="test.html">TEST SITE</a></li>
				<li><a class="log" href="logout.php">Log out</a></li>
			</ul>		
		</nav>

		<nav class="gamelst">
			<ul>
				<li><a href="tipping.php">Speed Typing</a></li>
				<li><a href="allforms.php"> Form page</a></li>
				<li><a href="#">Coming soon</a></li>
			</ul>
		</nav>
	
		<ul class="circles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
			

	</body>

</html>
