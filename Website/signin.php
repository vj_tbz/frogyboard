<?php
session_start();
include('config.php');

$email = "";
$password = "";
$email_err = "";
$password_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    //email validation
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter an email.";
    } else {
        $sql = "SELECT email FROM user WHERE email = ?";
        //$link -> Database connection
        if($stmt = mysqli_prepare($link, $sql)){

            mysqli_stmt_bind_param($stmt,"s",$param_email);
            $param_email = trim($_POST["email"]);

            //execution of SQL
            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) > 0){
                    $email_err = "The account already exists.";
                } else {
                    $email = trim($_POST["email"]);
                }
            }
            mysqli_stmt_close($stmt);
        }
    }

    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } else {
        $password = trim($_POST["password"]);
    }

    if(empty($email_err) && empty($password_err)){
        $sql = "INSERT INTO user (email, password, bestwps, accuracy) VALUES (?, ?, ?, ?)";

        if($stmt = mysqli_prepare($link, $sql)){


            mysqli_stmt_bind_param($stmt, "ssdd", $param_email, $param_password, $Nullo, $Nullo);
            $param_email = $email;
            $crypt_pass = password_hash($password, PASSWORD_DEFAULT);
            $param_password = $crypt_pass;
            $Nullo = 0;

            
            
            if(mysqli_stmt_execute($stmt) ){
                       
                // Redirect to login page
                header("location: login.html");
                    
            } else{
                echo "Hoppla :(";
            }


            

            // Close statement
            mysqli_stmt_close($stmt);

        }
    }

    mysqli_close($link);
}


?>

<!DOCTYPE html>
<html lang="de-CH">
<head>
    <meta charset="UTF-8">
    <title>Registrieren</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/sign.css">
    <style>
        body{ font: 14px sans-serif; }
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>
<body>
    <header>
    <h1>FrogyBoard</h1>
    </header>

    <div class="container">
        <div class="wrapper">
            <h2 class="title">Sign up</h2>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <div class="form-group">
                    <input type="text" name="email" placeholder="Email" class="form-control <?php echo (!empty($email_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $email; ?>">
                    <span class="invalid-feedback"><?php echo $email_err; ?></span>
                    
                </div>    
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
                    <span class="invalid-feedback"><?php echo $password_err; ?></span>
                    
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Registrieren">
                    <input type="reset" class="btn btn-outline-success" value="Reset">
                </div>
                <p>Already have an account? <a href="login.html">Log in</a>.</p>
            </form>
        </div>  
    </div>
      
</body>
</html>