feather.replace();

const big = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
const small = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];


document.addEventListener("keydown", event => {
  var keyPressed = event.key;
  var keyElement = "";
  

  var arrIndexSmall = small.indexOf(keyPressed);
  if(arrIndexSmall !== -1){
    keyElement = document.getElementById(keyPressed);
  } else {
      var arrIndexBig = big.indexOf(keyPressed);
      if(arrIndexBig !== -1){
        keyElement = document.getElementById(small[arrIndexBig]);
      } else {
        keyElement = document.getElementById(keyPressed);
      }
  }

    
  keyElement.classList.add("tap")
  keyElement.addEventListener('animationend', () => {
    keyElement.classList.remove("tap")
  })
})

