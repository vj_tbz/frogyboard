<?php
session_start();   
?>
<!doctype html>
<html lang="de-CH">

<head>
    <meta charset="uft-8">
    <title>LOG IN | FrogyBoard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/login.css">
</head>

<body>
    <header>
        <h1>FrogyBoard</h1>
    </header>

    <nav id="mainnav">

    </nav>

    <!-- PHP CODE -->
    <?php
            $loginstatus = false;
            $name = htmlspecialchars($_POST['uname']);
            $pw = htmlspecialchars($_POST['psw']);
            
            // php - MySQL connection
            include('db_inc.php');
            $dsn = 'mysql:host=' . $host . ';dbname=' . $database;
            $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'];
            include('connect.php');

            $db -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            $query = "SELECT email, password FROM user";
            $result = $db -> query($query);

            $resarr = $result -> fetchAll();
            $result = NULL;
            $db = NULL;
            
            $counter = 0;
            $arrayName = array();
            $arrayPW = array();
            $pass_bool = false;

            foreach($resarr as $row){
                $counter = 0;
                foreach($row as $val){
                    switch($counter){
                        case 0: 
                            $arrayName[] = $val;
                            break;
                        case 1:
                            if(!$pass_bool){
                                $pass_bool = password_verify($pw, $val);
                            }
                            break;
                        default:
                            break;    
                    }      
                    $counter = $counter + 1;

                }
            }

            if (in_array($name, $arrayName) && $pass_bool) {
                $_SESSION["email"]= $name;
                $loginstatus = true;
                header("location: tipping.php");
                exit;
            };
            

        ?>

    <main>
    <div class="container">
				<div class="form">
					<h2 class="title">Login</h2>
					<article id = "log">
						
						<form accept-charset="utf-8" action="phpconnect.php" method="post" id="enterlog">
								<div class="form-group">
									<input type="text" name="uname" id="username" required>
									<label for="username">Username</label>
								</div>
								<div class="form-group">
									<input type="password" name="psw" id="psw" required>
									<label for="psw">Password</label>
								</div>	
			
								<input class="submit" id = "logbutton" type="Submit"  value="Submit">
								<div class="row">
									<p>Not Yet Registered? <a href="signin.php">Sign Up</a></p>
								</div>
							</div>
						</form>
                        <?php 
                            if($loginstatus) {?>
                            Login successfully
                            <?php }else{ ?>
                            Username or Password were wrong, try again
                        <?php } ?>   
					</article>

				</div>
	

			</div>
		   
    </main>

    <footer>
        <div class="text-center p-3">
            <p>
                &copy; 2023, FrogyBoard </p>

        </div>
    </footer>


</body>

</html>