<?php 
session_start();

if (!isset($_SESSION['email'])) {
    // Redirect to the login page or show an error
    header("Location: login.html");
    exit;
}

$_SESSION["prev-Seite"] = "tipping"; 

?>

<html lang="en">
<head>
  <title>Simple Speed Typer</title>
  
  <link rel="stylesheet" href="assets/css/keyboard.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/logout.css">
  
  <script src="https://unpkg.com/feather-icons"></script>
  
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Playpen+Sans:wght@300&display=swap" rel="stylesheet">
</head>
<body>

  <nav class="mainnav">
    <ul>
      <li><a class="log" href="mode.php">Home</a></li>
      <li><a class="log" href="logout.php">Log out</a></li>
    </ul>		
	</nav>

  <h1>Test your Speed</h1>

  <div class="results">
    <div class="square">
      <div class="header_text">WPM</div>
      <div class="res_wpm">100</div>
    </div>
    <div class="square">
      <div class="header_text">Errors</div>
      <div class="res_errors">0</div>
    </div>
    <div class="square">
      <div class="header_text">Time</div>
      <div class="res_time">0s</div>
    </div>
    <div class="square">
      <div class="header_text">% Accuracy</div>
      <div class="res_accuracy">0</div>
    </div>
  </div>

  <div class="container">
    <div class="quote">Click on the area below to start the game.</div>
    <button class="restart_btn" onclick="startGame()">Restart</button>
    <button class="home" onclick="window.location='mode.php';"> Home </button>
  </div>

  <div class="stats">
    <div class="errors">
      <div class="header_text">Errors</div>
      <div class="curr_errors">0</div>
    </div>
    <div class="timer">
      <div class="header_text">Time</div>
      <div class="curr_time">0s</div>
    </div>
    <div class="accuracy">
      <div class="header_text">% Accuracy</div>
      <div class="curr_accuracy">0</div>
    </div>
  </div>

  <div class="keyboard">
    <div class="row">
      <div class="key key__esc" id="Escape">
        <i data-feather="x"></i>
      </div>
      <div class="key key__symbols" id="1">
        ! <span> 1
      </div>
      <div class="key key__symbols" id="2">
        @ <span> 2
      </div>
      <div class="key key__symbols" id="3">
        # <span> 3
      </div>
      <div class="key key__symbols" id="4">
        $ <span> 4
      </div>
      <div class="key key__symbols" id="5">
        % <span> 5
      </div>
      <div class="key key__symbols" id="6">
        ^ <span> 6
      </div>
      <div class="key key__symbols" id="7">
        & <span> 7
      </div>
      <div class="key key__symbols" id="8">
        * <span> 8
      </div>
      <div class="key key__symbols" id="9">
        ( <span> 9
      </div>
      <div class="key key__symbols" id="0">
        ) <span> 0
      </div>
      <div class="key key__symbols" >
        _ <span> -
      </div>
      <div class="key key__symbols" >
        + <span> =
      </div>
      <div class="key key__delete key__icon" id="Backspace">
        <i data-feather="delete"></i>
      </div>
    </div>

    
    <div class="row">
      <div class="key key__oneandhalf" id="Tab">TAB</div>
      <div class="key" id="q">Q</div>
      <div class="key" id="w">W</div>
      <div class="key" id="e">E</div>
      <div class="key" id="r">R</div>
      <div class="key" id="t">T</div>
      <div class="key" id="z">Z</div>
      <div class="key" id="u">U</div>
      <div class="key" id="i">I</div>
      <div class="key" id="o">O</div>
      <div class="key" id="p">P</div>
      <div class="key key__symbols">
        { <span> [
      </div>
      <div class="key key__symbols">
        } <span> ]
      </div>
      <div class="key key__symbols key__oneandhalf">
        | <span> \
      </div>
    </div>


    <div class="row">
      <div class="key key__caps" id="caps">
        <i data-feather="meh"></i>
      </div>
      <div class="key" id="a">A</div>
      <div class="key" id="s">S</div>
      <div class="key" id="d">D</div>
      <div class="key" id="f">F</div>
      <div class="key" id="g">G</div>
      <div class="key" id="h">H</div>
      <div class="key" id="j">J</div>
      <div class="key" id="k">K</div>
      <div class="key" id="l">L</div>
      <div class="key key__symbols" >
        : <span> ;
      </div>
      <div class="key key__symbols" >
        " <span> '
      </div>
      <div class="key key__enter" id="Enter">
        <i data-feather="corner-down-left"></i>
      </div>
    </div>


    <div class="row">
      <div class="key key__shift-left" id="Shift">
        <i data-feather="arrow-up-circle"></i>
      </div>
      <div class="key" id="y">Y</div>
      <div class="key" id="x">X</div>
      <div class="key" id="c">C</div>
      <div class="key" id="v">V</div>
      <div class="key" id="b">B</div>
      <div class="key" id="n">N</div>
      <div class="key" id="m">M</div>
      <div class="key key__symbols" id=",">
        > <span> .
      </div>
      <div class="key key__symbols" id=".">
        < <span> .
      </div>
      <div class="key key__symbols" >
        ? <span> /
      </div>
        <div class="key">
          <i data-feather="arrow-up-circle"></i>
        </div>
        <div class="key key__arrow">
          <i data-feather="arrow-up"></i>
        </div>
        <div class="key">
          <i data-feather="trash-2"></i>
        </div>
    </div>

    <div class="row">
      <div class="key key__bottom-funct">
        ^
      </div>
      <div class="key key__bottom-funct">
        <i data-feather="activity"></i>
      </div>
      <div class="key key__bottom-funct">
        <i data-feather="command"></i>
      </div>
      <div class="key key__spacebar">
      </div>
      <div class="key">
        <i data-feather="command"></i>
      </div>
      <div class="key">
        <i data-feather="activity"></i>
      </div>
      <div class="key key__arrow">
        <i data-feather="arrow-left"></i>
      </div>
      <div class="key key__arrow">
        <i data-feather="arrow-down"></i>
      </div>
      <div class="key key__arrow">
        <i data-feather="arrow-right"></i>
      </div>
    </div>

  </div>

  <script src="assets/javascript/keyboard.js"></script>
  <script src="assets/javascript/game.js"></script>

</body>
</html>