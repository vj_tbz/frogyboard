var quotes = [
    "Kafka is a member of the Stellaron Hunters who is calm, collected, and beautiful. Her record on the wanted list of the Interastral Peace Corporation only lists her name and her hobby. People have always imagined her to be elegant, respectable, and in pursuit of things of beauty even in combat.",
    "Alhaitham is a member of the Haravatat of the Sumeru Akademiya and the Akademiya's Scribe, responsible for documenting their findings and drafting ordinances. Despite his brilliance, people within the Akademiya are perplexed by Alhaitham due to his unwillingness to aspire for higher office in spite of his egocentricity, and many have been at odds with him for his uncompromising view on rationality and the truth",
    "Joseph inherited the Ripple and its capabilities from his grandfather, Jonathan Joestar, as well as his mother, Lisa Lisa. In addition, he uses techniques commonly present in stage magic to confuse his foes. After his Ripple training, Joseph frequently combines his Ripple with other objects to perform complex and creative attacks. Later in Stardust Crusaders, he shows that he is able use the Ripple in conjunction with his Stand, Hermit Purple.This content comes from JoJo's Bizarre Encyclopedia (https://jojowiki.com), and must be attributed to its authors if you are using it on another wiki or web page, as specified in the license."
  ];
  
let quoteText = document.querySelector(".quotetxt");
  
function generateQuote() {

    var randomIndex = Math.floor(Math.random() * quotes.length);
    var quote = quotes[randomIndex];
    quoteText.innerHTML = quote;

}






