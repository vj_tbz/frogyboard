// define the time limit
let TIME_LIMIT = 1000000;

// define quotes to be used
let quotes_array = [ 
  "Kafka is a member of the Stellaron Hunters who is calm, collected, and beautiful. Her record on the wanted list of the Interastral Peace Corporation only lists her name and her hobby. People have always imagined her to be elegant, respectable, and in pursuit of things of beauty even in combat.",
  "Alhaitham is a member of the Haravatat of the Sumeru Akademiya and the Akademiya's Scribe, responsible for documenting their findings and drafting ordinances. Despite his brilliance, people within the Akademiya are perplexed by Alhaitham due to his unwillingness to aspire for higher office in spite of his egocentricity.",
  "Joseph Joestar is a fictional character in the Japanese manga series JoJo's Bizarre Adventure, written and illustrated by Hirohiko Araki. Joseph is the main protagonist of the series' second story arc, Battle Tendency, and the grandson of the first arc's protagonist, Jonathan Joestar."
  ];

// selecting required elements
let timer_text = document.querySelector(".curr_time");
let accuracy_text = document.querySelector(".curr_accuracy");
let error_text = document.querySelector(".curr_errors");
let res_timer = document.querySelector(".res_time");
let res_accuracy = document.querySelector(".res_accuracy");
let res_error = document.querySelector(".res_errors");
let res_wpm = document.querySelector(".res_wpm");
let quote_text = document.querySelector(".quote");
let restart_btn = document.querySelector(".restart_btn");
let keyboard = document.querySelector(".keyboard");
let home_btn = document.querySelector(".home");
let stats = document.querySelector(".stats");
let results = document.querySelector(".results");

let timeLeft = TIME_LIMIT;
let timeElapsed = 0;
let total_errors = 0;
let errors = 0;
let accuracy = 0;
let characterTyped = 0;
let current_quote = "";
let quoteNo = 0;
let timer = null;

var inp_arr = [];
var q_arr = [];
var counter = 0;


function updateQuote() {
  quote_text.textContent = null;
  quoteNo = Math.floor(Math.random() * quotes_array.length);
  current_quote = quotes_array[quoteNo];

  // separate each character and make an element 
  // out of each of them to individually style them
  current_quote.split('').forEach(char => {
    const charSpan = document.createElement('span')
    charSpan.innerText = char
    quote_text.appendChild(charSpan)
    q_arr.push(char);
  })

  // roll over to the first quote
  if (quoteNo < quotes_array.length - 1)
    quoteNo++;
  else
    quoteNo = 0;
}

function updateTimer() {
  if (timeLeft > 0) {
    // decrease the current time left
    timeLeft--;

    // increase the time elapsed
    timeElapsed++;
    
    // update the timer text
    timer_text.textContent = timeElapsed + "s";
    res_timer.textContent = timeElapsed + "s";
  }
}

function finishGame() {
  // stop the timer
  clearInterval(timer);

  // show finishing text
  //quote_text.textContent = "Click on restart to start a new game.";

  // display restart button
  restart_btn.style.display = "block";

  // calculate cpm and wpm
  wpm = Math.round((((characterTyped / 5) / timeElapsed) * 60));

  // update cpm and wpm text
  res_wpm.textContent = wpm;

  //hide keyboard
  keyboard.style.display = "none";

  home_btn.style.display = "block";

  stats.style.display = "none";
  results.style.display = "flex";
  
}


function startGame() {

  resetValues();
  updateQuote();

  // clear old and start a new timer
  clearInterval(timer);
  timer = setInterval(updateTimer, 1000);
}

function resetValues() {
  timeLeft = TIME_LIMIT;
  timeElapsed = 0;
  errors = 0;
  total_errors = 0;
  accuracy = 0;
  characterTyped = 0;
  quoteNo = 0;
  inp_arr = [];
  q_arr = [];
  counter = 0;
  quote_text.textContent = 'Click on the area below to start the game.';
  accuracy_text.textContent = 100;
  timer_text.textContent = '0s';
  error_text.textContent = 0;
  restart_btn.style.display = "none";
  home_btn.style.display = "none";
  keyboard.style.display = "block";
  stats.style.display = "block";
  results.style.display = "none";  
}

document.addEventListener("keydown", event => {

  //Enter Key value: 'Enter'


  // get current input key and put in array
  var keyPressed = event.key;
  if(keyPressed == 'Shift'){
    //do nothing
  } else if(keyPressed == 'Backspace'){
    inp_arr.pop();
  } else {
    inp_arr.push(keyPressed);
  }
  
    // increment total characters typed
    characterTyped++;
  
    errors = 0;
  
    quoteSpanArray = quote_text.querySelectorAll('span');
    
    quoteSpanArray.forEach((char,index) => {
      
      let typedChar = inp_arr[index];


      // characters not currently typed
      if (typedChar == null) {
        char.classList.remove('correct_char');
        char.classList.remove('incorrect_char');
        char.classList.remove('current_char');
  
        // correct characters
      } else if (typedChar === char.innerText) {
        char.classList.add('correct_char');
        char.classList.remove('incorrect_char');
        char.classList.remove('current_char');
  
        // incorrect characters
      } else {
        char.classList.add('incorrect_char');
        char.classList.remove('correct_char');
        char.classList.remove('current_char');

  
  
        // increment number of errors
        errors++;
      }

      if(inp_arr.length == index){
        char.classList.add('current_char');
      }

    });

    //display #errors
    error_text.textContent = total_errors + errors;
    res_error.textContent = total_errors + errors;
    let correctCharacters = characterTyped - errors;

    if (restart_btn.style.display == 'none'){
      // update accuracy text
      let accuracyVal = ((correctCharacters / characterTyped) * 100);
      accuracy_text.textContent = Math.round(accuracyVal);
      res_accuracy.textContent = Math.round(accuracyVal);
    }


    // if current text is completely typed
    // irrespective of errors
    if (inp_arr.length == current_quote.length) {

      // update total errors
      total_errors += errors;

      finishGame();

    }
 
});


startGame();