# FrogyBoard - Changelog


## 06.09.2023
- Created Repository
- Created Base Stucture
- Tried one Animated Background
  - User J Liked it very much
- [x] change of background color needed :3

## 07.09.2023
- Changed Background Color
  - To be reviewed by user J.

## 13.09.2023
- Added login.html
  - added logic to connect to an example DB

## 20.09.2023
- Added empty tipping.html, rating.html, signin.html and profil.html
- Changed description of the README (project description)
  
## 13.09.2023
- Added 2 Dummy Buttons, need to be formatted more
  - Hover animation per CSS @keyframes (line swoop under the text)
  - [X] Check if the same hover animation is possible with JavaScript

## 14.09.2023
- Actually; The line Animation was done with CSS smoothly XD
  - Source: https://stackoverflow.com/questions/40242378/underline-from-left-to-right-on-hover-in-and-out
- Created test html and css to experiment code snippets

## 20.09.2023
- Formatted buttons for emergency
- First merge into dev :0

## 21.09.2023
- Random Text generator prototype created in test.html and textgen.js

## 05.10.2023
- fixing abomination >:0
- now program ends when one text is finished
- restart is operating smoothly
- keyboard disappears in the statistics
- keyboard appears again by restart
- added home button to the statistics
  - I think we need a in-between page for Home and game panel
- 

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/vj_tbz/frogyboard.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/vj_tbz/frogyboard/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***



